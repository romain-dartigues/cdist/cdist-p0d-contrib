cdist-type__p0d_clear-linux(7)
==============================

NAME
----
cdist-type__p0d_clear-linux - TODO


DESCRIPTION
-----------
This space intentionally left blank.


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------
None.


BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

    # TODO
    __p0d_clear-linux


SEE ALSO
--------
:strong:`TODO`\ (7)


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

cdist-type__firecracker(7)
==========================

NAME
----
cdist-type__firecracker - install firecracker


DESCRIPTION
-----------
This type install `firecracker micro VM`_ binaries.


REQUIRED PARAMETERS
-------------------
version
   Specify the version to install.


OPTIONAL PARAMETERS
-------------------
None.


BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

   __firecracker --version 1.6.0

   __firecracker --state absent


SEE ALSO
--------
n/a

.. _firecracker micro VM: https://firecracker-microvm.github.io/

AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

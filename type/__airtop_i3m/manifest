#!/bin/sh -e
#
# 2024 Romain Dartigues (romain.dartigues@gmail.com)
#
# This file is part of cdist.
#
# cdist is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cdist is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cdist. If not, see <http://www.gnu.org/licenses/>.
#


os=$(cat "$__global/explorer/os")
airtop=$(cat "$__type/explorer/airtop")

if [ -z "$airtop" ]
then
	printf "Your machine is not supported by this type (%s)\n" "${__type##*/}" >&2
	exit 1
fi

case "$os" in
centos|debian|devuan|redhat|ubuntu) : ;;
*)
	printf "Your operating system (%s) is currently not supported by this type (%s)\n" "$os" "${__type##*/}" >&2
	printf "Please contribute an implementation for it if you can.\n" >&2
	exit 1
	;;
esac

__download /usr/local/sbin/airtop-fpsvc \
	--url https://fit-iot.com/files/download/airtop/sw/I3M/Linux/airtop-fpsvc \
	--onchange 'chmod -c 0755 /usr/local/sbin/airtop-fpsvc'

[ -z "${airtop##*nvidia*}" ] &&
__download /usr/local/sbin/gpu-thermald \
	--url https://fit-iot.com/files/download/airtop/sw/I3M/Linux/gpu-thermald \
	--onchange 'chmod -c 0755 /usr/local/sbin/gpu-thermald'

__link '/usr/lib/x86_64-linux-gnu/libsensors.so.4' \
	--source 'libsensors.so.5.0.0' \
	--type symbolic

__file '/etc/rc.local' \
	--state present \
	--mode 0755 \
	--onchange 'sed -i "1i#!/bin/sh" /etc/rc.local'

_modprobe='modprobe i2c-i801 && sleep 1 && /usr/local/sbin/airtop-fpsvc'

require="__file/etc/rc.local" \
__line rc:modprobe \
	--file '/etc/rc.local' \
	--after '^#.*!.*/.*sh$' \
	--line "$_modprobe" \
	--onchange "$_modprobe"

_nvidia='[ -e /proc/driver/nvidia/gpus ] && [ -x /usr/local/sbin/gpu-thermald ] && /usr/local/sbin/gpu-thermald'
require="__line/rc:modprobe" \
__line rc:gpu \
	--file '/etc/rc.local' \
	--after '/airtop-fpsvc$' \
	--line "$_nvidia" \
	--onchange "$_nvidia"


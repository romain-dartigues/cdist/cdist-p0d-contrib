cdist-type__airtop_i3m(7)
=========================

NAME
----
cdist-type__airtop_i3m - Install Airtop front panel


DESCRIPTION
-----------
This type install the I³M Linux daemon on Airtop3 and should work with Airtop2.


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------
None.


BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

    __airtop_i3m


SEE ALSO
--------
:abbr:`I³M (Integrated Interactive Information Monitor)`,
https://fit-pc.com/wiki/index.php?title=Installing_I3M_Linux_Daemon_on_Airtop3


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

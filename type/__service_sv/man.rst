cdist-type__service_sv(7)
=========================

NAME
----
cdist-type__service_sv - Manage services runit services


DESCRIPTION
-----------
Manage services managed by ``runsv``.


REQUIRED PARAMETERS
-------------------
None.

OPTIONAL PARAMETERS
-------------------
name
    String which will used as name instead of the object id.

action
    Arbitrary parameter passed as action.
    Usually 'up', 'down', 'restart'; see man :strong:`sv`\ (8).

state
    Enable or disable the service.

EXAMPLES
--------

.. code-block:: sh

    # service must run
    __service_sv nginx --action up

    # service must stopped
    __service_sv sshd --action down

   # service must be started and enabled
   __service_sv smartd --action up --state enable


SEE ALSO
--------
:strong:`cdist-type__service`\ (7),
:strong:`sv`\ (8),
http://smarden.org/runit/


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

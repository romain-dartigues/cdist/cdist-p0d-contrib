cdist-type__p0d_gitlab-runner(7)
================================

NAME
----
cdist-type__p0d_gitlab-runner - setup a `GitLab Runner`_


DESCRIPTION
-----------
This setup a Docker-based GitLab Runner.

This type expect a properly configured Docker server on the host.


REQUIRED PARAMETERS
-------------------
mode
   Either "docker" or "manual".

   "docker" [#]_
      Install GitLab Runer as a Docker container.
      This is the recommended method, as it is arguably easier to update.

   "manual" [#]_
      Perform a "manual installation" of the GitLab Runner on the system.

.. [#] https://docs.gitlab.com/runner/install/docker.html
.. [#] https://docs.gitlab.com/runner/install/linux-manually.html

# https://docs.gitlab.com/runner/configuration/advanced-configuration.html


OPTIONAL PARAMETERS
-------------------
baseurl
   Used only for the "manual" mode, the base URL where to download the gitlab-runner static binary.

register
   gitlab-runner list

BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

    __p0d_gitlab-runner --mode docker


SEE ALSO
--------
:strong:`cdist-type__p0d_docker`\ (7),
:strong:`cdist-type__docker`\ (7),
`GitLab Runner`_.

.. _GitLab Runner: https://docs.gitlab.com/runner/


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

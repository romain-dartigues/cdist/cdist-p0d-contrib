cdist-type__package_xbps(7)
===========================

NAME
----
cdist-type__package_xbps - Manage packages with XBPS


DESCRIPTION
-----------
The X Binary Package System (XBPS) is a fast package manager that has is usually used on the Void Linux distribution.


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------
name
   If supplied, use the name and not the object id as the package name.

state
    Either "present" or "absent", defaults to "present"

onchange
   The code to run if line is added, removed or updated.

EXAMPLES
--------

.. code-block:: sh

    # Ensure zsh in installed
    __package_xbps zsh --state present

    # Remove package
    __package_xbps apache --state absent


SEE ALSO
--------
:strong:`cdist-type__package`\ (7)


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

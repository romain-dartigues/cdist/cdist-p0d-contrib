cdist-type__p0d_essentials(7)
=============================

NAME
----
cdist-type__p0d_essentials - Essentials for p0d


DESCRIPTION
-----------
Install essential packages for p0d and optionally set a system-wide configuration.


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------
bat:
	If set, specify the bat_ version to install.

fd:
	If set, specify the fd_ version to install.

jq:
	If set, specify the jq_ version to install.

rg:
	If set, specify the ripgrep_ version to install.

.. _bat: https://github.com/sharkdp/bat/releases
.. _fd: https://github.com/sharkdp/fd/releases
.. _jq: https://github.com/jqlang/jq/releases
.. _ripgrep: https://github.com/BurntSushi/ripgrep/releases

BOOLEAN PARAMETERS
------------------
configure:
	If set, configure the system-wide configuration.


EXAMPLES
--------

.. code-block:: sh

   # Install specified versions of programs, without changing the system-wide configuration.
   __p0d_essentials

   # Same, but also configure the tools on the system.
   __p0d_essentials --configure

   # Do not install bat nor fd
   __p0d_essentials --bat='' --fd=''


SEE ALSO
--------
:strong:`cdist-type__p0d_clean_ubuntu`\ (7)


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

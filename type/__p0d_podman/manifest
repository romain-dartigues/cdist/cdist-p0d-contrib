#!/bin/sh -e
#
# 2024 Romain Dartigues (romain.dartigues@gmail.com)
#
# This file is part of cdist.
#
# cdist is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cdist is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cdist. If not, see <http://www.gnu.org/licenses/>.
#

os=$(cat "$__global/explorer/os")

case "$os" in
	debian|ubuntu) : ;;
	*)
		printf "Your operating system (%s) is currently not supported by this type (%s)\n" "$os" "${__type##*/}" >&2
		printf "Please contribute an implementation for it if you can.\n" >&2
		exit 1
		;;
esac

__apt_update_index

# TODO
#if [ -f "${__object}/parameter/unstable" ]
#then
#	curl -fsSL "https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_$(lsb_release -rs)/Release.key" |
#	gpg --dearmor > /etc/apt/keyrings/devel_kubic_libcontainers_unstable.gpg
#	echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/devel_kubic_libcontainers_unstable.gpg] https://download.opensuse.org/repositories/devel:kubic:libcontainers:unstable/xUbuntu_$(lsb_release -rs)/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:unstable.list
#fi

for package in \
	bsdextrautils \
	containernetworking-plugins \
	podman-docker \
	python3-pip
do require="__apt_update_index" __package "$package" --state=present
done

require="__package/python3-pip" \
__package_pip --pip pip3 "docker-compose"

find "${__type}/files" -type f -printf '%04m %P\n' |
while read -r mode filename
do __file "/${filename}" --state exists --source "${__type}/files/${filename}" --mode "$mode"
done

require="__package/podman-docker" \
__block /etc/containers/registries.conf \
	--prefix '[registries.search]' \
	--text 'registries = ["ghcr.io", "docker.pkg.github.com", "quay.io"]'

require="__package/podman-docker" \
__file /etc/containers/nodocker

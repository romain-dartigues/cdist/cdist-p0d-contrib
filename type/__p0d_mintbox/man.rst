cdist-type__p0d_mintbox(7)
==========================

NAME
----
cdist-type__p0d_mintbox - quickly set up *my* workspace on a graphical workstation


DESCRIPTION
-----------
Quickly set up my workspace on a graphical (Linux Mint) workstation.


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------
None.


BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

    __p0d_mintbox


SEE ALSO
--------
:strong:`cdist-type__p0d_debian_reset`\ (7),
:strong:`cdist-type__install_github_release_bin`\ (7)


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2021 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

#!/bin/sh

set -u

# Type parameters.
sunstone_domain=$(cat "${__object:?}/parameter/sunstone-domain")
one_release=$(cat "${__object:?}/parameter/one-release")

os=$(cat "${__global:?}/explorer/os")
case "$os" in
almalinux|debian|redhat|ubuntu)
	__opennebula_repo --one-release "${one_release}"
	;;
*)
	printf "Your operating system (%s) is currently not supported by this type (%s)\n" "$os" "${__type##*/}" >&2
	printf "Please contribute an implementation for it if you can.\n" >&2
	exit 1
esac

for service in opennebula opennebula-sunstone opennebula-fireedge opennebula-guac
do
	require="__opennebula_repo" __package "$service"
	require="__package/opennebula" __start_on_boot "$service"
done
require="__opennebula_repo" __package opennebula-gate
require="__opennebula_repo" __package opennebula-flow
require="__opennebula_repo" __package opennebula-provision

#__package ruby-ldap

# Templating parameters.
#export ONE_MYSQL_PASSWORD=$(cat "${__object:?}/parameter/mysql-password")
export FIREEDGE_DOMAIN=$(cat "${__object:?}/parameter/fireedge-domain")

#LDAP_SERVERS=$(cat "${__object:?}/parameter/ldap-server")
#LDAP_BIND_DN=$(cat "${__object:?}/parameter/ldap-bind-dn")
#LDAP_BIND_PW=$(cat "${__object:?}/parameter/ldap-bind-pw")
#LDAP_REQUIRES_GROUP=$(cat "${__object:?}/parameter/ldap-requires-group")
#export LDAP_SERVERS LDAP_BIND_DN LDAP_BIND_PW LDAP_REQUIRES_GROUP
#export ONED_DEFAULT_AUTH=ldap

export SUNSTONE_VNC_PROXY_CERT="/etc/nginx/ssl/$sunstone_domain/fullchain.pem"
export SUNSTONE_VNC_PROXY_KEY="/etc/nginx/ssl/$sunstone_domain/privkey.pem"

# Installing and configuration MySQL database backend.
#__package mariadb-server
#require="__package/mariadb-server" __mysql_database opennebula \
#	--user "oneadmin" \
#	--password ${ONE_MYSQL_PASSWORD:?}

# Generate and deploy ONE configuration.
mkdir -p "${__object:?}/files"
"${__type:?}/files/oned.conf.sh" > "${__object:?}/files/oned.conf"
"${__type:?}/files/sunstone-server.conf.sh" > "${__object:?}/files/sunstone-server.conf"
"${__type:?}/files/fireedge-server.conf.sh" > "${__object:?}/files/fireedge-server.conf"
#"${__type:?}/files/ldap_auth.conf.sh" > "${__object:?}/files/ldap_auth.conf"

require="__package/opennebula" __file /etc/one/oned.conf \
	--source "${__object:?}/files/oned.conf" \
	--group oneadmin --mode 0640 \
	--onchange "service opennebula restart"

require="__package/opennebula __package/ruby-ldap" __file /etc/one/auth/ldap_auth.conf \
	--source "${__object:?}/files/ldap_auth.conf" \
	--group oneadmin --mode 0640 \
	--onchange "service opennebula restart"
require="__package/opennebula" __file /var/lib/one/ldap_group_mapping.yaml \
	--owner "oneadmin" \
	--source - <<- EOF
		cn=opennebula-admin,ou=groups,dc=recycled,dc=cloud: 0
		EOF
require="__package/opennebula-sunstone __nginx/$sunstone_domain" \
	__file /etc/one/sunstone-server.conf \
	--source "${__object:?}/files/sunstone-server.conf" \
	--group oneadmin --mode 0640 \
	--onchange "service opennebula-sunstone restart"
require="__package/opennebula-fireedge __nginx/$FIREEDGE_DOMAIN" \
	__file /etc/one/fireedge-server.conf \
	--source "${__object:?}/files/fireedge-server.conf" \
	--group oneadmin --mode 0640 \
	--onchange "service opennebula-fireedge restart"
require="__package/opennebula-guacd" \
	__file /etc/one/guacd \
	--group oneadmin --mode 0640 \
	--onchange "service opennebula-guacd restart" \
	--source - <<- EOF
	OPTS=""
	EOF

# Nginx as proxy.
require="__package/opennebula" __nginx "$sunstone_domain" \
	--force-cert-ownership-to www-data:oneadmin \
	--config - <<- EOF
	location / {
		proxy_http_version 1.1;
		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto \$scheme;
		proxy_set_header Host \$http_host;

		# See https://nginx.org/en/docs/http/websocket.html
		proxy_set_header Upgrade \$http_upgrade;
		proxy_set_header Connection "upgrade";

		proxy_pass http://localhost:9869;
	}
	EOF

require="__package/opennebula" __nginx "${FIREEDGE_DOMAIN:?}" \
	--config - <<- EOF
	location / {
		proxy_http_version 1.1;
		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Proto \$scheme;
		proxy_set_header Host \$http_host;

		# See https://nginx.org/en/docs/http/websocket.html
		proxy_set_header Upgrade \$http_upgrade;
		proxy_set_header Connection "upgrade";

		proxy_pass http://localhost:2616;
	}
	EOF

# README

I got tired doing one git repository per [cdist][] [type][],
this repository intend to contain p0d (public) types (prefixed by `__p0d_`)
and other general types.

Questions, suggestions are welcome.

[cdist]: https://www.cdi.st/
[type]: https://www.cdi.st/manual/latest/cdist-type.html

## Usage

One simple way to use it is to clone this repository then add it's path to the `cdist` configuration directory.

Example: through the CLI with `cdist {config,info,...} --conf-dir $path_to_this_directory`
or by adding it to your [cdist-configuration][] file:

```ini
[GLOBAL]
conf_dir = /path/to/this/directory
```

[cdist-configuration]: https://www.cdi.st/manual/latest/cdist-configuration.html

## Types

### __airtop_i3m

Install software for the [I³M][] module on [Compulab][] Airtop computers.

[Compulab]: https://www.compulab.com/
[I³M]: https://fit-pc.com/wiki/index.php?title=Airtop:I3M

### __firecracker

Install [firecracker micro VM][] static binaries, from the [GitHub release](https://github.com/firecracker-microvm/firecracker).

[cdist-type__firecracker(7)](./__firecracker/man.rst)

[firecracker micro VM]: http://firecracker-microvm.io/

### __p0d_clean_ubuntu

Clean an *Ubuntu 22.04 Minimal Server Installation*.

[cdist-type__p0d_clean_ubuntu(7)](./__p0d_clean_ubuntu/man.rst)

### __p0d_debian_reset
### __p0d_docker
### __p0d_essentials

This is a meta-type (which might be a manifest instead).
It does install essential packages for p0d and optionally set a system-wide configuration.

[cdist-type__p0d_essentials(7)](./__p0d_essentials/man.rst)

### __p0d_gitlab-runner

Setup a [GitLab runner][] in a p0d environment.

[cdist-type__p0d_gitlab-runner(7)](./__p0d_gitlab-runner/man.rst)

[GitLab runner]: https://docs.gitlab.com/runner/

### __p0d_mintbox

This is a meta-type (which might be a manifest instead),
to quickly set up *my* workspace on a graphical (Linux Mint) workstation.

[cdist-type__p0d_mintbox(7)](./__p0d_mintbox/man.rst)

### __p0d_podman

Setup [podman][], p0d style.

[podman]: https://podman.io/

[cdist-type__p0d_podman(7)](./__p0d_podman/man.rst)

### __p0d_xcp

## See also

* https://code.ungleich.ch/ungleich-public/cdist-contrib/
